# Create user for testing
CREATE USER IF NOT EXISTS 'userTest'@'%' IDENTIFIED BY 'userTest@2020';
# Create test databases
CREATE DATABASE IF NOT EXISTS dbTest;
# Grant privileges
GRANT ALL ON dbTest.* TO 'userTest'@'%' IDENTIFIED BY 'userTest@2020';

