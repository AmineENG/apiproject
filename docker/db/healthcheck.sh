#!/bin/bash

mysql --user=root -proot@2020 -e 'show databases' | grep dbTest > /dev/null 2>&1
if [ $? -ne 0 ] ; then
        echo "healthcheck ERROR - - DATABASE dbTest doesn't exist !!"
        exit 1
fi

echo "healthcheck SUCCESS"
exit 0