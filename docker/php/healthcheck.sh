#!/bin/bash

SYMFONY_VERSION=`/bin/bash -c 'php /var/www/app/bin/console --version'`

if [[ ${SYMFONY_VERSION} == "Symfony 4.4"* ]] ; then
        echo "healthcheck SUCCESS - $SYMFONY_VERSION"
        exit 0
fi

echo "healthcheck ERROR - SYMFONY_VERSION=$SYMFONY_VERSION"
exit 1