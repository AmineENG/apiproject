#!/bin/bash

cd /var/www/app

appEnv=${SYMFONY_ENV:=prod}

if [[ -d "vendor" ]]
then
    rm -rf vendor
fi

rm -rf var/log/* var/cache/* var/exports/*

# Install vendor
if [[ ${appEnv} == "prod" ]] ; then
    composer install --no-interaction --no-dev --no-cache --optimize-autoloader --classmap-authoritative
    composer dump-autoload --no-dev --classmap-authoritative
else
    composer install --no-interaction --no-cache
fi

# Setup app env
composer dump-env ${appEnv}

# Migrat database
php bin/console doctrine:migrations:migrate --env=${appEnv} -n --allow-no-migration

# Load fixture (Loaded also in production env for test reasons)
php bin/console doctrine:fixtures:load --append --env=${appEnv}

# Clear app cache
php bin/console cache:clear --env=${appEnv}

chown -R 1000.1000 .

chmod -R 777 var/cache var/log

# Execute original entrypoint
exec docker-php-entrypoint "$@"