<?php

namespace App\Exception;

use Throwable;

/**
 * Class EntityNotFountException
 *
 * @package App\Exception
 */
class EntityNotFountException extends ExporterContainerException
{
    protected $code = -1;

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->message = sprintf("[%s] : Entity not found !.",$message);
    }
}