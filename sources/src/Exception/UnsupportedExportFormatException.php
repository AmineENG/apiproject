<?php

namespace App\Exception;

/**
 * Class UnsupportedExportFormatException
 *
 * @package App\Exception
 */
class UnsupportedExportFormatException extends ExporterContainerException
{
   protected $code = -4;

   public function __construct($message = "", $code = 0, Throwable $previous = null)
   {
      parent::__construct($message, $code, $previous);
      $this->message = sprintf("[%s] : Unsupported export format !!", $message);
   }
}