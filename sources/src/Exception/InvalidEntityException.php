<?php

namespace App\Exception;

class InvalidEntityException extends ExporterContainerException
{
    protected $code = -2;

    protected $message = "Entity not valid !";

   public function __construct($message = "", $code = 0, Throwable $previous = null)
   {
      parent::__construct($message, $code, $previous);
      $this->message = sprintf("[%s] : Invalid Doctrine entity !",$message);
   }
}