<?php


namespace App\Exception;

/**
 * Class UnexportableCsvException
 *
 * @package App\Exception
 */
class UnexportableCsvException extends ExporterContainerException
{
   protected $code = -4;

   /**
    * UnexportableCsvException constructor.
    *
    * @param  string          $message
    * @param  int             $code
    * @param  Throwable|null  $previous
    */
   public function __construct($message = "", $code = 0, Throwable $previous = null)
   {
      parent::__construct($message, $code, $previous);
      $this->message = sprintf("[%s] : Unexportable entity. The entity repository must implement CsvExportableInterface.",$message);
   }

}