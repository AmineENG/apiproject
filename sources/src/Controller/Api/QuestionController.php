<?php

namespace App\Controller\Api;

use App\Entity\Question;
use App\Form\Api\QuestionType;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class ProductController
 *
 * @package App\Controller\Rest
 */
class QuestionController extends AbstractApiController
{
   /**
    * Serialization groupes
    */
   const SERIALIZATION_GROUPS = ['question'];

   /**
    * Get questions
    * @Rest\View(serializerGroups={"question"})
    * @Rest\Get("/questions",methods={"GET"})
    * @QueryParam(name="offset", requirements="\d+", default="", description="Pagenation offset")
    * @QueryParam(name="limit", requirements="\d+", default="", description="Elements maximum")
    * @QueryParam(name="sort", requirements="asc|desc", description="Elements sort (based on id)")
    *
    * @param  ParamFetcher  $paramFetcher
    *
    * @return View
    */
   public function getQuestions(ParamFetcher $paramFetcher) : View
   {
      $questions = $this->getDoctrine()->getManager()->getRepository(Question::class)->getCollection(
        $paramFetcher->all()
      );

      return self::generateView($questions, Response::HTTP_OK, self::SERIALIZATION_GROUPS);
   }

   /**
    * Get questions
    * @Rest\Get("/questions/{questionId}",methods={"GET"})
    *
    * @param  int|null  $questionId
    *
    * @return View
    */
   public function getQuestion(int $questionId = null) : View
   {
      $question = $this->getDoctrine()->getManager()->getRepository(Question::class)->find($questionId);

      if (!$question) {
         return self::generateView([], Response::HTTP_NOT_FOUND);
      }

      return self::generateView($question, Response::HTTP_OK, self::SERIALIZATION_GROUPS);
   }

   /**
    * Post question
    * @Rest\Post("/questions",methods={"POST"})
    * @Rest\View(serializerGroups={"question"})
    *
    * @param  Request  $request
    *
    * @return View
    */
   public function postQuestion(Request $request) : View
   {
      $question = new Question();

      $data = json_decode($request->getContent(), true);

      $form = $this->createForm(QuestionType::class, $question);

      $form->submit($data);

      if ($form->isValid() && $form->isSubmitted()) {
         $em = $this->getDoctrine()->getManager();

         $em->persist($question);
         $em->flush();

         return self::generateView($question, Response::HTTP_CREATED, self::SERIALIZATION_GROUPS);
      }

      return self::generateView($form, Response::HTTP_BAD_REQUEST);
   }

   /**
    * Put question
    * @Rest\Put("/questions/{questionId}",methods={"PUT"})
    * @Rest\View(serializerGroups={"question"})
    *
    * @param  Request  $request
    * @param  int      $questionId
    *
    * @return View
    */
   public function putQuestion(Request $request, int $questionId) : View
   {
      $em = $this->getDoctrine()->getManager();

      $question = $em->getRepository(Question::class)->find($questionId);

      if (!$question) {
         return self::generateView([], Response::HTTP_NOT_FOUND);
      }

      $data = json_decode($request->getContent(), true);

      $form = $this->createForm(QuestionType::class, $question);

      $form->submit($data);

      if ($form->isValid() && $form->isSubmitted()) {
         $em = $this->getDoctrine()->getManager();

         $em->persist($question);
         $em->flush();

         return self::generateView($question, Response::HTTP_OK, self::SERIALIZATION_GROUPS);
      }

      return self::generateView($form, Response::HTTP_BAD_REQUEST);
   }

   /**
    * Patch question
    * @Rest\Patch("/questions/{questionId}",methods={"PATCH"})
    * @Rest\View(serializerGroups={"question"})
    *
    * @param  Request  $request
    * @param  int      $questionId
    *
    * @return View
    */
   public function patchQuestion(Request $request, int $questionId) : View
   {
      $em = $this->getDoctrine()->getManager();

      $question = $em->getRepository(Question::class)->find($questionId);

      if (!$question) {
         return self::generateView([], Response::HTTP_NOT_FOUND);
      }

      $data = json_decode($request->getContent(), true);

      $form = $this->createForm(QuestionType::class, $question);

      $form->submit($data, false);

      if ($form->isValid() && $form->isSubmitted()) {
         $em = $this->getDoctrine()->getManager();

         $em->persist($question);
         $em->flush();

         return self::generateView($question, Response::HTTP_OK, self::SERIALIZATION_GROUPS);
      }

      return self::generateView($form, Response::HTTP_BAD_REQUEST);
   }

   /**
    * Delete question
    * @Rest\Delete("/questions/{questionId}",methods={"DELETE"})
    * @Rest\View(serializerGroups={"question"})
    *
    * @param  Request  $request
    * @param  int      $questionId
    *
    * @return View
    */
   public function deleteQuestion(Request $request, int $questionId) : View
   {
      $em = $this->getDoctrine()->getManager();

      $question = $em->getRepository(Question::class)->find($questionId);

      if (!$questionId) {
         self::generateView([], Response::HTTP_NOT_FOUND);
      }

      $em->remove($question);
      $em->flush();

      return self::generateView([], Response::HTTP_OK);
   }
}
