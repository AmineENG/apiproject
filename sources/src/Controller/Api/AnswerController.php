<?php

namespace App\Controller\Api;

use App\Entity\Answer;
use App\Form\Api\AnswerType;
use Exception;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class AnswerController
 *
 * @package App\Controller\Api
 */
class AnswerController extends AbstractApiController
{
   /**
    * Get answers
    * @Rest\Get("/answers",methods={"GET"})
    *
    * @QueryParam(name="offset", requirements="\d+", description="Pagenation offset")
    * @QueryParam(name="limit", requirements="\d+", description="Elements maximum")
    * @QueryParam(name="sort", requirements="asc|desc", description="Elements sort (based on id)")
    *
    * @param  ParamFetcher  $paramFetcher
    *
    * @return View
    */
   public function getAnswers(ParamFetcher $paramFetcher) : View
   {
      $answers = $this->getDoctrine()->getManager()->getRepository(Answer::class)->getCollection($paramFetcher->all());

      return self::generateView($answers, Response::HTTP_OK, ["answer"]);
   }

   /**
    * Get answer
    * @Rest\Get("/answers/{answerId}",methods={"GET"})
    *
    * @param  int  $answerId
    *
    * @return View
    */
   public function getAnswer(int $answerId) : View
   {
      $answer = $this->getDoctrine()->getManager()->getRepository(Answer::class)->find($answerId);

      if (!$answer) {
         return self::generateView([], Response::HTTP_NOT_FOUND);
      }

      return self::generateView($answer, Response::HTTP_OK, ["answer"]);
   }

   /**
    * Create answer
    * @Rest\Post("/answers",methods={"POST"})
    *
    * @param  Request  $request
    *
    * @return View
    * @throws Exception
    */
   public function postAnswer(Request $request) : View
   {
      $answer = new answer();

      $data = json_decode($request->getContent(), true);

      $form = $this->createForm(AnswerType::class, $answer);

      $form->submit($data);

      if ($form->isValid() && $form->isSubmitted()) {
         $em = $this->getDoctrine()->getManager();

         $em->persist($answer);
         $em->flush();

         return self::generateView($answer, Response::HTTP_CREATED, ["answer"]);
      }

      return self::generateView($form, Response::HTTP_BAD_REQUEST);
   }

   /**
    * Put answer
    * @Rest\Put("/answers/{answerId}",methods={"PUT"})
    *
    * @param  Request  $request
    * @param  int      $answerId
    *
    * @return View
    */
   public function putAnswer(Request $request, int $answerId) : View
   {
      $em = $this->getDoctrine()->getManager();

      $answer = $em->getRepository(Answer::class)->find($answerId);

      if (!$answer) {
         return self::generateView([], Response::HTTP_NOT_FOUND);
      }

      $data = json_decode($request->getContent(), true);

      $form = $this->createForm(AnswerType::class, $answer);

      $form->submit($data);

      if ($form->isValid() && $form->isSubmitted()) {

         $em = $this->getDoctrine()->getManager();
         $em->persist($answer);
         $em->flush();

         return self::generateView($answer, Response::HTTP_OK, ["answer"]);
      }

      return self::generateView($form, Response::HTTP_BAD_REQUEST);
   }

   /**
    * Patch answer (partial update)
    * @Rest\Patch("/answers/{answerId}",methods={"PATCH"})
    *
    * @param  Request  $request
    * @param  int      $answerId
    *
    * @return View
    */
   public function patchAnswer(Request $request, int $answerId) : View
   {
      $em = $this->getDoctrine()->getManager();

      $answer = $em->getRepository(Answer::class)->find($answerId);

      if (!$answer) {
         return self::generateView([], Response::HTTP_NOT_FOUND);
      }

      $data = json_decode($request->getContent(), true);

      $form = $this->createForm(AnswerType::class, $answer);

      $form->submit($data, false);

      if ($form->isValid() && $form->isSubmitted()) {
         $em = $this->getDoctrine()->getManager();

         $em->persist($answer);
         $em->flush();

         return self::generateView($answer, Response::HTTP_OK, ["answer"]);
      }

      return self::generateView($form, Response::HTTP_BAD_REQUEST);
   }

   /**
    * Delete answer
    * @Rest\Delete("/answers/{answerId}",methods={"DELETE"})
    *
    * @param  Request  $request
    * @param  int      $answerId
    *
    * @return View
    */
   public function deleteAnswer(Request $request, int $answerId) : View
   {
      $em = $this->getDoctrine()->getManager();

      $answer = $em->getRepository(Answer::class)->find($answerId);

      if (!$answer) {
         return self::generateView([], Response::HTTP_NOT_FOUND);
      }

      $em->remove($answer);
      $em->flush();

      return self::generateView([], Response::HTTP_OK);
   }
}
