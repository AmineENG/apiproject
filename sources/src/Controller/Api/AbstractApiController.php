<?php

namespace App\Controller\Api;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;

/**
 * Class RestController
 * @package App\Controller\Rest
 */
abstract class AbstractApiController extends AbstractFOSRestController
{
    /**
     * Generate view with serialization groups
     *
     * @param mixed $data
     * @param $statusCode
     * @param array $serializationGroupes
     * @return View
     */
    static function generateView($data, $statusCode, array $serializationGroupes = ["Default"]): View
    {
        $view = View::create($data, $statusCode);
        $context = new Context();
        $context->setGroups($serializationGroupes);
        $view->setContext($context);

        return $view;
    }
}
