<?php

namespace App\EventListener;

use App\Entity\Question;

use App\Entity\QuestionHistoric;
use DateTime;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\ORMException;

/**
 * Class QuestionHistorizer
 *
 * @package App\EventListener
 */
class QuestionHistorizerListener
{
    const TRACABLE_FIELDS = ['title','status','updated'];

   /**
    * @var array
    */
    private $questionHistorics = [];

   /**
    * Post update question entity
    *
    * @param  LifecycleEventArgs  $args
    */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $question = $args->getEntity();

        if(!$question instanceof Question)
            return;

        $changesSet = $args->getEntityManager()->getUnitOfWork()->getEntityChangeSet($question);

        foreach ($changesSet as $field => $values){
            if(in_array($field,self::TRACABLE_FIELDS))
            {
               $newValue = $values[1];
               $questionHistoric = new QuestionHistoric();
               $questionHistoric->setField($field);

               if ($newValue instanceof DateTime) {
                  $newValue = $newValue->format('Y-m-d H:i:s.u');
               }
               $questionHistoric->setNewValue($newValue);
               $question->addQuestionHitoric($questionHistoric);

               $this->questionHistorics[] = $questionHistoric;
            }
        }
    }

   /**
    * Post flush
    *
    * @param  PostFlushEventArgs  $postFlushEventArgs
    *
    * @throws ORMException
    */
    public function postFlush(PostFlushEventArgs $postFlushEventArgs)
    {
       if (!empty($this->questionHistorics)) {
          $em = $postFlushEventArgs->getEntityManager();
          foreach ($this->questionHistorics as $questionHistoric) {
             $em->persist($questionHistoric);
          }
          $this->questionHistorics = [];
          $em->flush();
       }
    }
}