<?php

namespace App\Command;

use App\Exception\EntityNotFountException;
use App\Exception\UnexportableCsvException;
use App\Service\Model\ExporterContainerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class ExportCommand
 *
 * @package App\Command
 */
class ExportCommand extends Command
{
   /**
    * Errors codes
    */
   const ENTITY_NOT_FOUND = -1;

   protected static $defaultName = 'app:export-data';

   /**
    * @var ExporterContainerInterface
    */
   private $exporterContainer;

   /**
    * ExportCsvCommand constructor.
    *
    * @param  ExporterContainerInterface  $exporterContainer
    * @param  string|null                 $name
    */
   public function __construct(ExporterContainerInterface $exporterContainer, string $name = null)
   {
      parent::__construct($name);
      $this->exporterContainer = $exporterContainer;
   }

   protected function configure()
   {
      $this
        ->setDescription('Export entity data.')
        ->addArgument('entityName', InputArgument::REQUIRED, 'Entity class name')
        ->addArgument('format', InputArgument::REQUIRED, 'Entity class name');
   }

   /**
    * Execute
    *
    * @param  InputInterface   $input
    * @param  OutputInterface  $output
    *
    * @return int
    * @throws ExceptionInterface
    */
   protected function execute(InputInterface $input, OutputInterface $output) : int
   {
      $io = new SymfonyStyle($input, $output);

      $className = $input->getArgument('entityName');
      $format    = $input->getArgument('format');

      if (!$className) {
         $io->error('entityName arg is required !');

         return false;
      }

      if (!$format) {
         $io->error('format argument is required !');

         return false;
      }

      try {
         $fileInfo = $this->exporterContainer->export($className,$format);
      } catch (EntityNotFountException $entityNotFountException) {
         $io->error($entityNotFountException->getMessage());

         return $entityNotFountException->getCode();
      } catch (UnexportableCsvException $unexportableCsvException) {
         $io->error($unexportableCsvException->getMessage());

         return $unexportableCsvException->getCode();
      } catch (Exception $exception) {
         $io->error($exception->getMessage());

         return $exception->getCode();
      }

      $io->success(sprintf('Export %s Success for entity %s : %s', mb_strtoupper($format) ,$className, $fileInfo['fileName']));

      return 0;
   }
}
