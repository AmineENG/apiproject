<?php

namespace App\Repository;

use App\Repository\Model\ApiEntityRepository;
use App\Repository\Model\CsvExportableInterface;
use Doctrine\ORM\Query;

/**
 * Class QuestionRepository
 *
 * @package App\Entity\Repository
 */
class QuestionRepository extends ApiEntityRepository implements CsvExportableInterface
{
   /**
    * @inheritDoc
    */
   public function getCsvExportData() : array
   {
      $query = $this->createQueryBuilder('q');

      return $query
        ->select('q')
        ->getQuery()
        ->getResult(Query::HYDRATE_ARRAY);
   }
}

