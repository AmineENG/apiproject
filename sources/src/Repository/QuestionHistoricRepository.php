<?php

namespace App\Repository;

use App\Repository\Model\CsvExportableInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Class QuestionRepository
 *
 * @package App\Entity\Repository
 */
class QuestionHistoricRepository extends EntityRepository implements CsvExportableInterface
{
   /**
    * @inheritDoc
    */
   public function getCsvExportData() : array
   {
      $query = $this->createQueryBuilder('q');

      return $query
        ->select('q')
        ->getQuery()
        ->getResult(Query::HYDRATE_ARRAY);
   }
}

