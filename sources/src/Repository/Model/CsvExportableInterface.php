<?php

namespace App\Repository\Model;

/**
 * Interface Exportable
 *
 * @package App\Repository\Model
 */
interface CsvExportableInterface
{
   /**
    * Get data for export
    *
    * @return mixed
    */
   public function getCsvExportData() : array ;
}