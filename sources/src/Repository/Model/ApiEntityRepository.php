<?php

namespace App\Repository\Model;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Class ApiEntityRepository
 *
 * @package App\Repository\Model
 */
abstract class ApiEntityRepository extends EntityRepository
{
   const FILTERS_OFFSET = "offset";
   const FILTERS_LIMIT  = "limit";
   const FILTERS_SORT   = "sort";

   /**
    * Get questions
    *
    * @param  array  $filters
    *
    * @return mixed
    */
   public function getCollection(array $filters = []){

      $qb = $this->createQueryBuilder('e')->select('e');

      if(!empty($filters[self::FILTERS_OFFSET])){
         $qb->setFirstResult($filters[self::FILTERS_OFFSET]);
      }

      if(!empty($filters[self::FILTERS_LIMIT])){
         $qb->setMaxResults($filters[self::FILTERS_LIMIT]);
      }

      if(!empty($filters[self::FILTERS_SORT])){
         $qb->addOrderBy('e.id',$filters[self::FILTERS_SORT]);
      }

      return $qb->getQuery()->getResult();
   }

   /**
    * Find all for CSV Export
    */
   protected function findAllForCsvExport()
   {
      $query = $this->createQueryBuilder('e');

      return $query
        ->select('e')
        ->getQuery()
        ->getResult(Query::HYDRATE_ARRAY);
   }
}

