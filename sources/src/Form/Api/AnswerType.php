<?php

namespace App\Form\Api;

use App\Entity\Answer;
use App\Entity\Question;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AnswerType
 *
 * @package App\Form\Api
 */
class AnswerType extends AbstractType
{
    /**
     * Build form
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('channel',TextType::class)
            ->add('body',TextType::class)
            ->add('question',EntityType::class,[
                'class' => Question::class
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                "data_class" => Answer::class,
                "csrf_protection" => false,
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'answer_form';
    }
}
