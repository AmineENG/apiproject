<?php

namespace App\Form\Api;

use App\Entity\Question;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class QuestionType
 *
 * @package App\Form\Api
 */
class QuestionType extends AbstractType
{
    /**
     * Build form
     *
     * @param  FormBuilderInterface  $builder
     * @param  array                 $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title',
                TextType::class
            )
            ->add(
                'status',
                TextType::class
            )
            ->add(
                'promoted'
            );
    }

    /**
     * @param  OptionsResolver  $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                "data_class"      => Question::class,
                "csrf_protection" => false,
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'question_form';
    }
}
