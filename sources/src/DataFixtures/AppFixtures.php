<?php

namespace App\DataFixtures;

use App\Entity\Answer;
use App\Entity\Question;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++){
            $question = new Question();
            $question->setPromoted($faker->boolean(50));
            $question->setTitle($faker->text(20));
            $question->setStatus($faker->randomElement([Question::STATUS_DRAFT,Question::STATUS_PUBLISHED]));

            $answer = new Answer();
            $answer->setChannel($faker->randomElement([Answer::CHANNEL_BOT,Answer::CHANNEL_FAQ]));
            $answer->setBody($faker->text(20));

            $question->addAnswer($answer);
            $manager->persist($answer);
            $manager->persist($question);
        }

        $manager->flush();
    }
}
