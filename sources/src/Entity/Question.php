<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use DateTime;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuestionRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="question")
 *
 */
class Question
{
   const STATUS_PUBLISHED = 'published';
   const STATUS_DRAFT     = 'draft';

   /**
    * @ORM\Column(type="integer")
    * @var int
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @JMS\Groups({"question","answer"})
    */
   private int $id;

   /**
    * @ORM\Column(type="string", length=255, nullable=false)
    * @var string
    * @Assert\NotNull()
    * @Assert\NotBlank()
    * @Assert\Type(
    *     type="string",
    *     message="The value {{ value }} is not a valid {{ type }}."
    * )
    * @Assert\Length(max="255",min="1")
    * @JMS\Groups({"question","answer"})
    */
   private $title;

   /**
    * @ORM\Column(type="boolean",nullable=false)
    * @var bool
    * @Assert\NotNull()
    * @JMS\Groups({"question","answer"})
    */
   private $promoted;

   /**
    * @ORM\Column(type="string", length=10 ,columnDefinition="enum('draft', 'published')", nullable=false)
    * @var string
    * @Assert\NotBlank()
    * @Assert\Type(
    *     type="string",
    *     message="The value {{ value }} is not a valid {{ type }}."
    *
    * )
    * @Assert\Length(max="10", min="5", groups={"default"})
    * @Assert\Choice(
    *     choices = { "draft", "published" },
    *     message = "Choose a valid status value (draft or published)"
    *
    * )
    * @JMS\Groups({"question","answer"})
    */
   private $status;

   /**
    * @var PersistentCollection
    * @ORM\OneToMany(targetEntity="Answer",mappedBy="question", cascade={"remove"})
    * @JMS\Groups({"question"})
    * @JMS\MaxDepth(1)
    */
   private $answers;

   /**
    * @var ArrayCollection
    * @ORM\OneToMany(targetEntity="QuestionHistoric",mappedBy="question", cascade={"remove"})
    */
   private $questionHistorics;

   /**
    * @var DateTime
    * @ORM\Column(type="datetime")
    * @JMS\MaxDepth(1)
    * @JMS\Groups({"question"})
    * @JMS\Expose()
    */
   protected $updated;

   /**
    * @var DateTime
    * @ORM\Column(type="datetime")
    * @JMS\Groups({"question"})
    * @JMS\Expose()
    */
   protected $created;

   /**
    * Question constructor.
    */
   public function __construct()
   {
      $this->created = new DateTime('now');
      $this->updated = new DateTime('now');

      $this->answers           = new ArrayCollection();
      $this->questionHistorics = new ArrayCollection();
   }

   /**
    * @return mixed
    */
   public function getId()
   {
      return $this->id;
   }

   /**
    * @return string|null
    */
   public function getTitle() : ?string
   {
      return $this->title;
   }

   /**
    * @param  string  $title
    */
   public function setTitle(?string $title) : void
   {
      $this->title = $title;
   }

   /**
    * @return bool
    */
   public function isPromoted() : ?bool
   {
      return $this->promoted;
   }

   /**
    * @param  bool  $promoted
    */
   public function setPromoted(bool $promoted) : void
   {
      $this->promoted = $promoted;
   }

   /**
    * @return string|null
    */
   public function getStatus() : ?string
   {
      return $this->status;
   }

   /**
    * @param  string  $status
    */
   public function setStatus(string $status) : void
   {
      $this->status = $status;
   }

   /**
    * @return ArrayCollection
    */
   public function getAnswers() : ArrayCollection
   {
      return $this->answers;
   }

   /**
    * @param  ArrayCollection  $answers
    */
   public function setAnswers(ArrayCollection $answers) : void
   {
      $this->answers = $answers;
   }

   /**
    * Add question historic
    *
    * @param  QuestionHistoric  $questionHistoric
    *
    * @return QuestionHistoric
    */
   public function addQuestionHitoric(QuestionHistoric &$questionHistoric) : QuestionHistoric
   {
      if (!$this->questionHistorics->contains($questionHistoric)) {
         $this->questionHistorics->add($questionHistoric);
         $questionHistoric->setQuestion($this);
      }

      return $questionHistoric;
   }

   /**
    * Add answer
    *
    * @param  Answer  $answer
    *
    * @return Answer
    */
   public function addAnswer(Answer &$answer) : Answer
   {
      if (!$this->answers->contains($answer)) {
         $this->answers->add($answer);
         $answer->setQuestion($this);
      }

      return $answer;
   }

   /**
    * @return DateTime
    */
   public function getUpdated() : DateTime
   {
      return $this->updated;
   }

   /**
    * @param  DateTime  $updated
    */
   public function setUpdated(DateTime $updated) : void
   {
      $this->updated = $updated;
   }

   /**
    * @return DateTime
    */
   public function getCreated() : DateTime
   {
      return $this->created;
   }

   /**
    * @param  DateTime  $created
    */
   public function setCreated(DateTime $created) : void
   {
      $this->created = $created;
   }

   /**
    * @ORM\PrePersist
    * @ORM\PreUpdate
    */
   public function setUpdatedAt()
   {
      $this->updated = new DateTime();
   }
}
