<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnswerRepository")
 * @ORM\Table(name="answer")
 * @JMS\ExclusionPolicy("all")
 */
class Answer
{
   const CHANNEL_FAQ = 'faq';
   const CHANNEL_BOT = 'bot';

   /**
    * @ORM\Column(type="integer")
    * @var int
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @JMS\Groups({"answer","question"})
    * @JMS\Expose()
    */
   private $id;

   /**
    * @ORM\Column(type="string", length=4 ,columnDefinition="enum('faq', 'bot')", nullable=false)
    * @var string
    * @Assert\NotBlank()
    * @Assert\Type(
    *     type="string",
    *     message="The value {{ value }} is not a valid {{ type }}."
    * )
    * @Assert\Length(max="3",min="3")
    * @JMS\Groups({"answer","question"})
    * @JMS\Expose()
    */
   private $channel;

   /**
    * @ORM\Column(type="string", length=255, nullable=false)
    * @var string
    * @Assert\NotBlank()
    * @Assert\Type(
    *     type="string",
    *     message="The value {{ value }} is not a valid {{ type }}."
    * )
    * @Assert\Length(max="255",min="1")
    * @JMS\Groups({"answer","question"})
    * @JMS\Expose()
    */
   private $body;

   /**
    * @var Question
    * @ORM\ManyToOne(targetEntity="Question", inversedBy="answers")
    * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
    * @JMS\Groups({"answer"})
    * @JMS\MaxDepth(1)
    * @JMS\Expose()
    */
   private $question;

   /**
    * @return mixed
    */
   public function getId()
   {
      return $this->id;
   }

   /**
    * @return string
    */
   public function getChannel() : ?string
   {
      return $this->channel;
   }

   /**
    * @param  string  $channel
    */
   public function setChannel(string $channel) : void
   {
      $this->channel = $channel;
   }

   /**
    * @return string
    */
   public function getBody() : ?string
   {
      return $this->body;
   }

   /**
    * @param  string  $body
    */
   public function setBody(?string $body) : void
   {
      $this->body = $body;
   }

   /**
    * @return Question
    */
   public function getQuestion() : ?Question
   {
      return $this->question;
   }

   /**
    * @param  Question  $question
    */
   public function setQuestion(Question $question) : void
   {
      $this->question = $question;
   }
}
