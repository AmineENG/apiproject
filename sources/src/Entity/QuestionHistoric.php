<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuestionHistoricRepository")
 * @ORM\Table(name="question_historic")
 * @JMS\ExclusionPolicy("all")
 *
 */
class QuestionHistoric
{
    /**
     * @ORM\Column(type="integer")
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10 ,columnDefinition="enum('title', 'status','updated')",nullable=false)
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max="10",min="5")
     *
     * @JMS\Expose()
     */
    private $field;

    /**
     * @ORM\Column(type="string", length=255 , nullable=false)
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(max="255",min="1")
     */
    private $newValue;

    /**
     * @var Question
     * @ORM\ManyToOne(targetEntity="Question")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    private $question;

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getField() : ?string
    {
        return $this->field;
    }

    /**
     * @param  string  $field
     */
    public function setField(string $field) : void
    {
        $this->field = $field;
    }

    /**
     * @return string
     */
    public function getNewValue() : ?string
    {
        return $this->newValue;
    }

    /**
     * @param  string  $newValue
     */
    public function setNewValue(string $newValue) : void
    {
        $this->newValue = $newValue;
    }

    /**
     * @return Question
     */
    public function getQuestion() : Question
    {
        return $this->question;
    }

    /**
     * @param  Question  $question
     */
    public function setQuestion(Question $question) : void
    {
        $this->question = $question;
    }
}
