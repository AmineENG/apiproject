<?php

namespace App\Service\Export;

use App\Exception\UnexportableCsvException;
use App\Repository\Model\CsvExportableInterface;
use App\Service\Model\AbstractExporter;
use App\Service\Model\EntitiesExporter;
use App\Service\Model\ExporterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Exception;
use DateTime;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class CsvExporter
 *
 * @package App\Service\Export
 */
class CsvExporter extends AbstractExporter implements ExporterInterface
{
   /**
    * Export format
    */
   private const FORMAT = 'csv';

   /**
    * @var string
    */
   private $exportFolder;

   /**
    * CsvEntitesExporter constructor.
    *
    * @param  EntityManagerInterface  $em
    * @param  Filesystem              $filesystem
    * @param  string                  $exportFolder
    */
   public function __construct(EntityManagerInterface $em, Filesystem $filesystem, $exportFolder)
   {
      parent::__construct($em, $filesystem);
      $this->exportFolder = $exportFolder;
      $this->serializer = new Serializer([new DateTimeNormalizer(), new ObjectNormalizer()], [new CsvEncoder()]);
   }

   /**
    * @inheritDoc
    */
   public function export(string $entityName) : array
   {
      $this->checkIfEntityExists($entityName);

      $entityRepository = $this->em->getRepository(self::ENTITIES_PATH . $entityName);

      $this->checkIfEntityExportable($entityRepository, $entityName);

      $data = $this->serializer->normalize(
        $entityRepository->getCsvExportData(),
        null,
        [DateTimeNormalizer::FORMAT_KEY => self::DATETIME_FORMAT]
      );

      $fileContent = $this->serializer->encode($data, self::FORMAT);

      return $this->saveAsFile($fileContent, $entityName);
   }

   /**
    * @inheritDoc
    */
   public function getFromat() : string
   {
      return self::FORMAT;
   }

   /**
    * Get file name
    *
    * @param $entityName
    *
    * @return string
    * @throws Exception
    */
   protected function getFileName($entityName) : string
   {
      $now = new DateTime('now');

      return $this->exportFolder  . $now->format('YmdHis') . '_' . $entityName . '.' . self::FORMAT;
   }

   /**
    * @inheritDoc
    * @throws UnexportableCsvException
    */
   protected function checkIfEntityExportable(ObjectRepository $entityRepository, string $entityName) : void
   {
      if (!$entityRepository instanceof CsvExportableInterface) {
         throw new UnexportableCsvException(self::ENTITIES_PATH . $entityName);
      }
   }

   /**
    * @inheritDoc
    */
   protected function saveAsFile($fileContent, string $entityName) : array
   {
      $fileName = $this->getFileName($entityName);

      if (!$this->fileSystem->exists($this->exportFolder)) {
         $this->fileSystem->mkdir($this->exportFolder);
      }

      $this->fileSystem->dumpFile($fileName, $fileContent);
      $this->fileSystem->chown($fileName, 1000);

      return [
        'fileName'    => $fileName,
        'fileContent' => $fileContent,
      ];
   }

   /**
    * @inheritDoc
    */
   public function support(string $format) : bool
   {
      return self::FORMAT === $format;
   }
}