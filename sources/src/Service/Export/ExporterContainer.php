<?php

namespace App\Service\Export;

use App\Exception\UnsupportedExportFormatException;
use App\Service\Model\ExporterInterface;
use App\Service\Model\ExporterContainerInterface;
use phpDocumentor\Reflection\Types\Mixed_;

/**
 * Class Exporter Generic data exporter
 *
 * @package App\Service\Export
 */
class ExporterContainer implements ExporterContainerInterface
{
   /**
    * @var array
    */
   private $exporters = [];

   /**
    * @inheritDoc
    */
   public function export(string $entityName, string $format) : array
   {
      foreach ($this->exporters as $exporter) {
         /*** @var ExporterInterface $exporter ** */
         if ($exporter->support($format)) {
            return $exporter->export($entityName);
         }
      }

      throw new UnsupportedExportFormatException($format);
   }

   /**
    * Add exporter
    *
    * @param  ExporterInterface  $exporter
    */
   public function addExporter(ExporterInterface $exporter) : void
   {
      $this->exporters[] = $exporter;
   }
}