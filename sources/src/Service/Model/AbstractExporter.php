<?php

namespace App\Service\Model;

use App\Exception\EntityNotFountException;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Exception;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class AbstractExporter
 *
 * @package App\Service\Model
 */
abstract class AbstractExporter
{
    /**
     * Export DateTime format for data serialization
     */
    protected const DATETIME_FORMAT = "Y-m-d H:i:s";

    /**
     * Entities class path
     */
    protected const ENTITIES_PATH = "App\Entity\\";

    /**
     * Export destination folder
     */
    protected const EXPORT_PATH = "/var/exports";

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var Filesystem
     */
    protected $fileSystem;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * EntitiesExporter constructor.
     *
     * @param  EntityManagerInterface  $em
     * @param  Filesystem              $fileSystem
     */
    public function __construct(EntityManagerInterface $em, Filesystem $fileSystem)
    {
        $this->em           = $em;
        $this->fileSystem   = $fileSystem;
    }

    /**
     * Get file name
     *
     * @param $entityName
     *
     * @return string
     * @throws Exception
     */
    abstract protected function getFileName($entityName) : string;

   /**
    * Check if entity exists
    *
    * @param  string  $entityName
    *
    * @return void
    * @throws EntityNotFountException
    */
    protected function checkIfEntityExists(string $entityName) : void
    {
        $entityName = self::ENTITIES_PATH . $entityName;

        if(!class_exists($entityName))
           throw new EntityNotFountException($entityName);
    }

   /**
    * Check if entity is exportable
    *
    * @param  ObjectRepository  $entityRepository
    * @param  string            $entityName
    */
    abstract protected function checkIfEntityExportable(ObjectRepository $entityRepository, string $entityName) : void ;

   /**
    * Save as file
    *
    * @param  mixed   $fileContent
    * @param  string  $entityName
    *
    * @return array
    * @throws Exception
    */
   abstract protected function saveAsFile($fileContent, string $entityName) : array ;
}