<?php


namespace App\Service\Model;

use App\Exception\EntityNotFountException;
use App\Exception\UnexportableCsvException;
use App\Exception\UnsupportedExportFormatException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Interface ExporterContainerInterface
 *
 * @package App\Service\Model
 */
interface ExporterContainerInterface
{
   /**
    * Export entities data
    *
    * @param  string  $entityName
    * @param  string  $format
    *
    * @return mixed
    * @throws UnexportableCsvException
    * @throws UnsupportedExportFormatException
    * @throws EntityNotFountException
    * @throws Exception
    * @throws ExceptionInterface
    */
   public function export(string $entityName, string $format);

   /**
    * Add exporter
    *
    * @param  ExporterInterface  $exporter
    */
   public function addExporter(ExporterInterface $exporter) : void;
}