<?php

namespace App\Service\Model;

use App\Exception\EntityNotFountException;
use App\Exception\UnexportableCsvException;
use Exception;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Interface ExporterInterface
 *
 * @package App\Service\Model
 */
interface ExporterInterface
{
   /**
    * Check if sertvice support format
    *
    * @param  string  $format
    *
    * @return bool
    */
   public function support(string $format) : bool;

   /**
    * Get exporter format
    *
    * @return string exporter format
    */
   public function getFromat() : string;

   /**
    * Export entity
    *
    * @param  string  $entityName
    *
    * @return array file path
    * @throws EntityNotFountException
    * @throws ExceptionInterface
    * @throws UnexportableCsvException
    * @throws Exception
    */
   public function export(string $entityName) : array;

}