<?php

namespace App\Tests\Command;

use App\Command\ExportCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class ExportCommandTest
 *
 * @package App\Tests\Command
 */
class ExportCommandTest extends KernelTestCase
{
   const COMMANDE_NAME = 'app:export-data';

   /**
    * Test with valid arg
    */
   public function testValidArgExecute()
   {
      $kernel      = static::createKernel();
      $application = new Application($kernel);

      $command       = $application->find(self::COMMANDE_NAME);
      $commandTester = new CommandTester($command);

      $commandTester->execute(
        [
          'entityName' => 'Question',
          'format'     => 'csv',
        ]
      );

      $output = $commandTester->getDisplay();

      $this->assertStringContainsString('[OK] Export CSV Success for entity Question', $output);
   }

   /**
    * Test with invalid arg
    */
   public function testInvalidArgExecute()
   {
      $kernel      = static::createKernel();
      $application = new Application($kernel);

      $command       = $application->find(self::COMMANDE_NAME);
      $commandTester = new CommandTester($command);

      $commandTester->execute(
        [
          'entityName' => 'Test',
          'format'     => 'csv',
        ]
      );

      $output = $commandTester->getDisplay();

      $this->assertEquals(ExportCommand::ENTITY_NOT_FOUND, $commandTester->getStatusCode());
      $this->assertStringContainsString('[App\Entity\Test] : Entity not found !', $output);
   }
}