<?php

namespace App\Tests\Controller\Api;

use App\Entity\Answer;
use App\Tests\Model\AppWebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class AnswerControllerTest
 *
 * @package App\Tests\Controller\Api
 */
class AnswerControllerTest extends AppWebTestCase
{

    public function testGetAnswers()
    {
        $client = static::createClient();

        $client->request(Request::METHOD_GET, '/api/answers');

        $response = $client->getResponse();

        $this->assertJsonResponse($response);
    }

    public function testGetAnswer()
    {
        $client = static::createClient();

        $client->request(Request::METHOD_GET, '/api/answers/1');

        $response = $client->getResponse();

        $data = json_decode($response->getContent(), true);

        $this->assertJsonResponse($response);

        $this->assertEquals(1, $data['id']);
    }

    public function testPostAnswers()
    {
        $client = static::createClient();

        $answer = [
            'channel'  => Answer::CHANNEL_BOT,
            'body'     => 'Php unit post test',
            'question' => 1,
        ];

        $client->request(
            Request::METHOD_POST,
            '/api/answers',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode($answer)
        );

        $response = $client->getResponse();

        $this->assertJsonResponse($response, Response::HTTP_CREATED);
    }

    public function testPutAnswers()
    {
        $client = static::createClient();

        $answer = [
            'channel'  => Answer::CHANNEL_BOT,
            'body'     => 'Php unit put test',
            'question' => 2,
        ];

        $client->request(
            Request::METHOD_POST,
            '/api/answers',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode($answer)
        );

        $postResponse = $client->getResponse();

        $this->assertJsonResponse($postResponse, Response::HTTP_CREATED);
        $answerPostResponseData = json_decode($postResponse->getContent(), true);

        $this->assertArrayHasKey('id', $answerPostResponseData);

        $answerPutRequestData = [
            'channel'  => Answer::CHANNEL_FAQ,
            'body'     => $answerPostResponseData['body'],
            'question' => intval($answerPostResponseData['question']),
        ];

        $client->request(
            Request::METHOD_PUT,
            '/api/answers/' . $answerPostResponseData['id'],
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode($answerPutRequestData)
        );

        $putResponse = $client->getResponse();

        $answerPutResponseData = json_decode($putResponse->getContent(), true);

        $this->assertJsonResponse($putResponse);
        $this->assertEquals($answerPostResponseData['id'], $answerPutResponseData['id']);
        $this->assertEquals($answerPostResponseData['body'], $answerPutResponseData['body']);
        $this->assertNotEquals($answerPostResponseData['channel'], $answerPutResponseData['channel']);
        $this->assertEquals($answerPutResponseData['channel'], Answer::CHANNEL_FAQ);
    }

    public function testPatchAnswers()
    {
        $client = static::createClient();

        $answer = [
            'channel'    => Answer::CHANNEL_BOT,
            'body'       => 'Php unit post test',
            'question'   => 3
        ];


        $client->request(
            Request::METHOD_POST,
            '/api/answers',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode($answer)
        );

        $postResponse = $client->getResponse();
        $this->assertJsonResponse($postResponse, Response::HTTP_CREATED);
        $answerPostResponseData = json_decode($postResponse->getContent(),true);

        $this->assertArrayHasKey('id',$answerPostResponseData);

        $answerPatchRequestData = [
            'channel'  => $answerPostResponseData['channel'],
            'body'     => 'Phpunit patch test body',
            'question' => intval($answerPostResponseData['question']),
        ];

        $client->request(
            Request::METHOD_PATCH,
            '/api/answers/' . $answerPostResponseData['id'],
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode($answerPatchRequestData)
        );

        $patchResponse = $client->getResponse();

        $answerPatchResponseData = json_decode($patchResponse->getContent(),true);

        $this->assertJsonResponse($patchResponse);
        $this->assertEquals($answerPostResponseData['id'],$answerPatchResponseData['id']);
        $this->assertEquals($answerPostResponseData['channel'],$answerPatchResponseData['channel']);
        $this->assertNotEquals($answerPostResponseData['body'],$answerPatchResponseData['body']);
        $this->assertEquals('Phpunit patch test body',$answerPatchResponseData['body']);
    }

    public function testDeletAnswers()
    {
        $client = static::createClient();

        $answer = [
            'channel'  => Answer::CHANNEL_BOT,
            'body'     => 'Php unit post test',
            'question' => 4,
        ];

        /**
         * Create Answers
         */
        $client->request(
            Request::METHOD_POST,
            '/api/answers',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode($answer)
        );

        $postResponse = $client->getResponse();
        $this->assertJsonResponse($postResponse, Response::HTTP_CREATED);
        $answerPostResponseData = json_decode($postResponse->getContent(), true);

        $this->assertArrayHasKey('id', $answerPostResponseData);
        /**
         * delete quetion
         */
        $client->request(
            Request::METHOD_DELETE,
            '/api/answers/' . $answerPostResponseData['id'],
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            ""
        );

        $deleteResponse = $client->getResponse();
        $this->assertJsonResponse($deleteResponse);

        $answerEntity = $this->em->getRepository(Answer::class)->find($answerPostResponseData['id']);

        $this->assertEquals($answerEntity, null);

    }
}