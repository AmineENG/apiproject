<?php

namespace App\Tests\Controller\Api;

use App\Entity\Question;
use App\Tests\Model\AppWebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class QuestionControllerTest
 *
 * @package App\Tests\Controller\Api
 */
class QuestionControllerTest extends AppWebTestCase
{
    public function testGetQuestions()
    {
        $client = static::createClient();

        $client->request(Request::METHOD_GET, '/api/questions');

        $response = $client->getResponse();

        $data = json_decode($response->getContent(),true);

        $this->assertCount(10,$data);

        $this->assertJsonResponse($response);
    }

    public function testGetQuestion()
    {
        $client = static::createClient();

        $client->request(Request::METHOD_GET, '/api/questions/1');

        $response = $client->getResponse();

        $data = json_decode($response->getContent(), true);

        $this->assertJsonResponse($response);

        $this->assertSame(1, $data['id']);
    }

    public function testPostQuestion()
    {
        $client = static::createClient();

        $question = [
            'title'    => 'phpunit test post',
            'status'   => Question::STATUS_DRAFT,
            'promoted' => true,
        ];

        $client->request(
            Request::METHOD_POST,
            '/api/questions',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode($question)
        );

        $response = $client->getResponse();

        $questionData = json_decode($response->getContent(),true);

        $this->assertJsonResponse($response, Response::HTTP_CREATED);
        $this->assertArrayHasKey('id',$questionData);
        $this->assertEquals(11,$questionData['id']);
    }

    public function testPutQuestion()
    {
        $client = static::createClient();

        $question = [
            'title'    => 'phpunit test post',
            'status'   => Question::STATUS_DRAFT,
            'promoted' => true,
        ];

        /**
         * Create question
         */
        $client->request(
            Request::METHOD_POST,
            '/api/questions',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode($question)
        );

        $postResponse = $client->getResponse();

        $this->assertJsonResponse($postResponse, Response::HTTP_CREATED);
        $questionPostResponseData = json_decode($postResponse->getContent(),true);

        $this->assertArrayHasKey('id',$questionPostResponseData);
        /**
         * Put quetion
         */
        $questionPutRequestData = array_slice($questionPostResponseData, 1, 3,true);
        $questionPutRequestData['title'] = 'Phpunit put test';

        $client->request(
            Request::METHOD_PUT,
            '/api/questions/' . $questionPostResponseData['id'],
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode($questionPutRequestData)
        );

        $putResponse = $client->getResponse();

        $questionPutResponseData = json_decode($putResponse->getContent(),true);

        $this->assertJsonResponse($putResponse);
        $this->assertSame($questionPostResponseData['id'],$questionPutResponseData['id']);
        $this->assertSame($questionPostResponseData['status'],$questionPutResponseData['status']);
        $this->assertSame($questionPostResponseData['promoted'],$questionPutResponseData['promoted']);
        $this->assertNotEquals($questionPostResponseData['title'],$questionPutResponseData['title']);
        $this->assertSame('Phpunit put test',$questionPutResponseData['title']);
    }

    public function testPatchQuestion()
    {
        $client = static::createClient();

        $question = [
            'title'    => 'phpunit post test',
            'status'   => Question::STATUS_PUBLISHED,
            'promoted' => true,
        ];

        /**
         * Create question
         */
        $client->request(
            Request::METHOD_POST,
            '/api/questions',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode($question)
        );

        $postResponse = $client->getResponse();
        $this->assertJsonResponse($postResponse, Response::HTTP_CREATED);
        $questionPostResponseData = json_decode($postResponse->getContent(),true);

        $this->assertArrayHasKey('id',$questionPostResponseData);
        /**
         * Put quetion
         */
        $questionPatchRequestData = array_slice($questionPostResponseData, 1, 3, true);
        $questionPatchRequestData['title'] = 'Phpunit patch test';

        $client->request(
            Request::METHOD_PATCH,
            '/api/questions/' . $questionPostResponseData['id'],
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode($questionPatchRequestData)
        );

        $patchResponse = $client->getResponse();

        $questionPatchResponseData = json_decode($patchResponse->getContent(),true);

        $this->assertJsonResponse($patchResponse);
        $this->assertSame($questionPostResponseData['id'],$questionPatchResponseData['id']);
        $this->assertSame($questionPostResponseData['status'],$questionPatchResponseData['status']);
        $this->assertSame($questionPostResponseData['promoted'],$questionPatchResponseData['promoted']);
        $this->assertNotEquals($questionPostResponseData['title'],$questionPatchResponseData['title']);
        $this->assertEquals('Phpunit patch test',$questionPatchResponseData['title']);
    }

    public function testDeletQuestion()
    {
        $client = static::createClient();

        $question = [
            'title'    => 'phpunit delete test',
            'status'   => Question::STATUS_PUBLISHED,
            'promoted' => true,
        ];

        /**
         * Create question
         */
        $client->request(
            Request::METHOD_POST,
            '/api/questions',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode($question)
        );

        $postResponse = $client->getResponse();
        $this->assertJsonResponse($postResponse, Response::HTTP_CREATED);
        $questionPostResponseData = json_decode($postResponse->getContent(),true);

        $this->assertArrayHasKey('id',$questionPostResponseData);
        /**
         * delete quetion
         */
        $client->request(
            Request::METHOD_DELETE,
            '/api/questions/' . $questionPostResponseData['id'],
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            ""
        );

        $deleteResponse = $client->getResponse();
        $this->assertJsonResponse($deleteResponse);

        $questionEntity = $this->em->getRepository(Question::class)->find($questionPostResponseData['id']);

        $this->assertEquals($questionEntity,null);

    }
}