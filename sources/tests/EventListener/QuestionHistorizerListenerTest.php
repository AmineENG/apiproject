<?php

namespace App\Tests\Service;

use App\Entity\Answer;
use App\Entity\Question;
use App\Entity\QuestionHistoric;
use App\Tests\Model\AppWebTestCase;
use Doctrine\DBAL\DBALException;

/**
 * Class QuestionManagerTest
 *
 * @package App\Tests\Service
 */
class QuestionHistorizerListenerTest extends AppWebTestCase
{
    /**
     * @throws DBALException
     */
    protected function setUp() : void
    {
        self::bootKernel();

        $this->em = self::$container->get('doctrine')->getManager();

        $this->truncateEntities([
            Answer::class,
            Question::class,
            QuestionHistoric::class
        ]);
    }

   /**
    * Test on post update title
    */
    public function testOnPostUpdateTitleQuestion()
    {
        $question = new Question();

        $question->setTitle('Title test');
        $question->setStatus(Question::STATUS_PUBLISHED);
        $question->setPromoted(true);

        $this->em->persist($question);
        $this->em->flush();
        $this->em->refresh($question);

        $this->assertNotNull($question->getId());

        $question->setTitle('Title test post-update');
        $this->em->flush();

        /**
         * @var $questionTitleHistorics QuestionHistoric
         */
        $questionTitleHistorics = $this->em->getRepository(QuestionHistoric::class)->findOneBy(['question' => $question,'field' => 'title']);

        $this->assertNotNull($questionTitleHistorics);
        $this->assertEquals('Title test post-update',$questionTitleHistorics->getNewValue());
        $this->assertNotNull($questionTitleHistorics->getQuestion());
        $this->assertEquals($question->getId(),$questionTitleHistorics->getQuestion()->getId());
    }

   /**
    * Test on post update status
    */
    public function testOnPostUpdateStatusQuestion()
    {
        $question = new Question();

        $question->setTitle('Title test');
        $question->setStatus(Question::STATUS_PUBLISHED);
        $question->setPromoted(true);

        $this->em->persist($question);
        $this->em->flush();
        $this->em->refresh($question);

        $this->assertNotNull($question->getId());

        $question->setStatus(Question::STATUS_DRAFT);
        $this->em->flush();

        /**
         * @var $questionStatusHistorics QuestionHistoric
         */
        $questionStatusHistorics = $this->em->getRepository(QuestionHistoric::class)->findOneBy(['question' => $question,'field' => 'status']);

        $this->assertNotNull($questionStatusHistorics);
        $this->assertEquals(Question::STATUS_DRAFT,$questionStatusHistorics->getNewValue());
        $this->assertNotNull($questionStatusHistorics->getQuestion());
        $this->assertEquals($question->getId(),$questionStatusHistorics->getQuestion()->getId());
    }

   /**
    * Test on post update updated
    */
    public function testOnPostUpdateUpdatedQuestion()
    {
        $question = new Question();

        $question->setTitle('Title test');
        $question->setStatus(Question::STATUS_PUBLISHED);
        $question->setPromoted(true);

        $this->em->persist($question);
        $this->em->flush();
        $this->em->refresh($question);

        $this->assertNotNull($question->getId());

        $question->setStatus(Question::STATUS_DRAFT);
        $this->em->flush();

        /**
         * @var $questionUpdatedHistorics QuestionHistoric
         */
        $questionUpdatedHistorics = $this->em->getRepository(QuestionHistoric::class)->findOneBy(['question' => $question,'field' => 'updated']);

        $this->assertNotNull($questionUpdatedHistorics);
        $this->assertEquals($question->getUpdated(),\DateTime::createFromFormat('Y-m-d H:i:s.u',$questionUpdatedHistorics->getNewValue()));
        $this->assertNotNull($questionUpdatedHistorics->getQuestion());
        $this->assertEquals($question->getId(),$questionUpdatedHistorics->getQuestion()->getId());
    }
}