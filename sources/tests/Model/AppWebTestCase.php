<?php

namespace App\Tests\Model;

use App\DataFixtures\AppFixtures;
use App\Entity\Answer;
use App\Entity\Question;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AppWebTestCase
 *
 * @package App\Tests\Model
 */
abstract class AppWebTestCase extends WebTestCase
{
   /**
    * @var EntityManagerInterface
    */
   protected $em;

   /**
    * Entities
    */
   const ENTITIES = [
     Answer::class,
     Question::class,
   ];

   /**
    * @throws DBALException
    */
   protected function setUp() : void
   {
      self::bootKernel();

      $this->em = self::$container->get('doctrine')->getManager();

      $this->truncateEntities(self::ENTITIES);

      $this->loadFixtures(
        [
          AppFixtures::class,
        ],
        true
      );
   }

   /**
    * Assert Json response
    *
    * @param  Response  $response
    * @param  int       $statusCode
    */
   protected function assertJsonResponse(Response $response, $statusCode = Response::HTTP_OK)
   {
      $this->assertTrue($response->headers->contains('Content-Type', 'application/json'));
      $this->assertSame($statusCode, $response->getStatusCode());
   }

   /**
    * Truncate entities
    *
    * @param  array  $entities
    *
    * @throws DBALException
    */
   protected function truncateEntities(array $entities)
   {
      $connection = $this->em->getConnection();
      $databasePlatform = $connection->getDatabasePlatform();

      if ($databasePlatform->supportsForeignKeyConstraints()) {
         $connection->query('SET FOREIGN_KEY_CHECKS=0');
      }

      foreach ($entities as $entity) {
         $query = $databasePlatform->getTruncateTableSQL(
           $this->em->getClassMetadata($entity)->getTableName()
         );
         $connection->executeUpdate($query);
      }

      if ($databasePlatform->supportsForeignKeyConstraints()) {
         $connection->query('SET FOREIGN_KEY_CHECKS=1');
      }
   }
}