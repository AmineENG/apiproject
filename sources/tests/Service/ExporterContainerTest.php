<?php

namespace App\Tests\Service;

use App\DataFixtures\AppFixtures;
use App\Entity\Answer;
use App\Entity\Question;
use App\Entity\QuestionHistoric;
use App\Exception\EntityNotFountException;
use App\Exception\UnexportableCsvException;
use App\Exception\UnsupportedExportFormatException;
use App\Service\CsvExporter;
use App\Service\Export\ExporterContainer;
use App\Service\Model\ExporterContainerInterface;
use App\Service\Model\ExporterInterface;
use App\Service\Model\QuestionManagerInterface;
use App\Service\QuestionManager;
use App\Tests\Model\AppWebTestCase;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class QuestionManagerTest
 *
 * @package App\Tests\Service
 */
class ExporterContainerTest extends AppWebTestCase
{
   /**
    * @var Filesystem
    */
   private $fileSystem;

   /**
    * @var ExporterContainerInterface
    */
   private $exporterContainer;

   /**
    * @throws DBALException
    */
   protected function setUp() : void
   {
      self::bootKernel();

      $this->em                = self::$container->get('doctrine')->getManager();
      $this->exporterContainer = self::$container->get(ExporterContainer::class);
      $this->fileSystem        = self::$container->get('filesystem');

      $this->truncateEntities(
        [
          Answer::class,
          Question::class,
          QuestionHistoric::class,
        ]
      );

      $this->loadFixtures(
        [
          AppFixtures::class,
        ],
        true
      );
   }

   /**
    * Test with valid arg
    *
    * @throws EntityNotFountException
    * @throws ExceptionInterface
    * @throws UnexportableCsvException
    * @throws UnsupportedExportFormatException
    */
   public function testValidEntityNameExport()
   {
      $fileInfo = $this->exporterContainer->export('Question', 'csv');

      $this->assertIsArray($fileInfo);
      $this->assertArrayHasKey('fileName', $fileInfo);
      $this->assertArrayHasKey('fileContent', $fileInfo);
      $this->assertFileExists($fileInfo['fileName']);

      $this->assertEquals($fileInfo['fileContent'], file_get_contents($fileInfo['fileName']));
   }

   /**
    * Test with invalid arg
    *
    * @throws EntityNotFountException
    * @throws ExceptionInterface
    * @throws UnexportableCsvException
    * @throws UnsupportedExportFormatException
    */
   public function testInalidEntityNameExport()
   {
      $this->expectException(EntityNotFountException::class);

      $this->exporterContainer->export('Test', 'csv');
   }

   /**
    * Test unsupported format
    *
    * @throws EntityNotFountException
    * @throws ExceptionInterface
    * @throws UnexportableCsvException
    * @throws UnsupportedExportFormatException
    */
   public function testUnsupportedFormatExport()
   {
      $this->expectException(UnsupportedExportFormatException::class);

      $this->exporterContainer->export('Question', 'pdf');
   }
}