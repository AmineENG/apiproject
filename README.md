# Demo api project

Demo Api/UnitTest project  

## Getting Started

Clone project : git clone git@gitlab.com:AmineENG/apiproject.git

### Prerequisites

```
- Linux environment (ubuntu or debian, etc..)
- Make (Makefile) installed (sudo apt-get install build-essential)
- Docker (>= 19.03.6 )
- Docker compose (>= 1.25.1)
```

### Building project

Production environment :

```
In root directory : make ENV=prod
```

Test environment :

```
In root directory : make ENV=test
```

Build project using test env start unit/functional tests immediately after build process

### Clean project

```
In root directory : make ENV=test clean (remove containers and named data volume)
```

## Running tests

Require that the project is already built in test environment => **make ENV=test**
 
```
In root directory : make ENV=test run-test 
```

## Api interaction

Import Postman collection ***app.postman_collection.json***

## Run data export cmd
 
```
In root directory : make ENV={env} ENTITY={entity} FORMAT={format} export-data 
```
Replace {entity} by Entity name (Question|Answer|QuestionHistoric), {format} by (csv|xml),  {env} by current running environment (prod|test)

### Coding style

```
Symfony Framework coding style
```

## Built With

* [PHP v7.4 fpm (FastCGI Process Manager)](https://php-fpm.org) - Web technologie used
* [Nginx v1.17.7](https://www.nginx.com) - Web server/reverse proxy
* [Symfony v4.4.4](https://symfony.com) - The web framework used
* [Composer](https://getcomposer.org) - Dependency Management

## Authors

* **Cherfi Amine**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Feature to complete the project

* Add api documentation (ApiDoc / Nelmio-swagger)
* Form validation tests
* Kubernetes deployement pipeline
* Use Redis for sessions/doctrine-cache storage 
* Loadbalancing strategy 
* Database replicate (Read/Write optimization)
