SHELL := /bin/bash

ifndef ENV
$(info Usage : make ENV=[test|prod])
$(error The ENV variable is missing.)
endif

build:
	$(info ------------- Building $(ENV) environment ------------------------------------)
	@make ENV=$(ENV) clean
	SF_ENV=$(ENV) docker-compose build
	@make ENV=$(ENV) start

start:
	$(info ------------- Starting $(ENV) environment ------------------------------------)
	SF_ENV=$(ENV) docker-compose up -d
    ifeq ($(ENV), test)
		@make ENV=test run-test
    endif
	$(info ------------- Build done : Api exposed on http://localhost/api ----------------)
	docker-compose exec -T -u 1000 php php bin/console debug:router
clean:
	$(info ------------- Cleaning $(ENV) environment -------------------------------------)
	docker-compose down -v

run-test:
    ifneq ($(ENV), test)
		$(error Make : Running test require test environment.)
    endif
	$(info ------------- Running Phpunit tests -------------------------------------------)
	docker-compose exec -T -u 1000 php php ./bin/phpunit -v --debug --colors=always

export-data:
    ifndef ENTITY
		$(error The ENTITY variable is missing.)
    endif
    ifndef FORMAT
		$(error The FORMAT variable is missing.)
    endif
	$(info ------------- Export csv : Entity $(ENTITY) -----------------------------------)
	docker-compose exec -T -u 1000 php php bin/console app:export-data $(ENTITY) $(FORMAT)








